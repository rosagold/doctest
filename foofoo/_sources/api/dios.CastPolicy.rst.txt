CastPolicy
==========

.. currentmodule:: dios

.. autoclass:: CastPolicy
   :show-inheritance:

   .. rubric:: Attributes Summary

   .. autosummary::

      ~CastPolicy.force
      ~CastPolicy.never
      ~CastPolicy.save

   .. rubric:: Attributes Documentation

   .. autoattribute:: force
   .. autoattribute:: never
   .. autoattribute:: save
